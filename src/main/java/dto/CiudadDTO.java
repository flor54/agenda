package dto;

public class CiudadDTO {
	private int id;
	private int id_provincia;

	private String nombre;
	public CiudadDTO(int id, String nombre, int id_provincia )
	{
		this.id = id;
		this.id_provincia = id_provincia;
		this.nombre = nombre;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getId_provincia() {
		return id_provincia;
	}
	public void setId_provincia(int id_provincia) {
		this.id_provincia = id_provincia;
	}
}
