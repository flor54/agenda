package modelo;

import java.util.List;

import dto.CiudadDTO;
import dto.PersonaDTO;
import dto.ProvinciaDTO;
import persistencia.dao.interfaz.CiudadDAO;
import persistencia.dao.interfaz.DAOAbstractFactory;
import persistencia.dao.interfaz.PersonaDAO;
import persistencia.dao.interfaz.ProvinciaDAO;


public class Agenda 
{
	private PersonaDAO persona;
	private ProvinciaDAO provincia;
	private CiudadDAO ciudad;	
	
	public Agenda(DAOAbstractFactory metodo_persistencia)
	{
		this.persona = metodo_persistencia.createPersonaDAO();
		this.provincia = metodo_persistencia.createProvinciaDAO();
		this.ciudad = metodo_persistencia.createCiudadDAO();
	}
	
	public void agregarPersona(PersonaDTO nuevaPersona)
	{
		this.persona.insert(nuevaPersona);
	}

	public void borrarPersona(PersonaDTO persona_a_eliminar) 
	{
		this.persona.delete(persona_a_eliminar);
	}
	
	public List<PersonaDTO> obtenerPersonas()
	{
		return this.persona.readAll();		
	}
	
	public List<ProvinciaDTO> obtenerProvincias()
	{
		return this.provincia.readAll();		
	}
	
	public List<CiudadDTO> obtenerCiudades(int id_provincia)
	{
		return this.ciudad.filter(id_provincia);		
	}
	
}
