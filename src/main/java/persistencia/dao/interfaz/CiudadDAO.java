package persistencia.dao.interfaz;

import java.util.List;

import dto.CiudadDTO;


public interface CiudadDAO {
	public List<CiudadDTO> readAll();

	public List<CiudadDTO> filter(int id_provincia);
}
