package persistencia.dao.interfaz;

import persistencia.dao.mysql.CiudadDAOSQL;
import persistencia.dao.mysql.ProvinciaDAOSQL;

public interface DAOAbstractFactory 
{
	public PersonaDAO createPersonaDAO();
	
	public ProvinciaDAOSQL createProvinciaDAO();
	
	public CiudadDAOSQL createCiudadDAO();
	
}
