package persistencia.dao.mysql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dto.CiudadDTO;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.CiudadDAO;

public class CiudadDAOSQL implements CiudadDAO{
	private static final String readall = "SELECT * FROM ciudad WHERE id_provincia = ? ORDER BY nombre";

	@Override
	public List<CiudadDTO> readAll() {
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		
		ArrayList<CiudadDTO> provincias = new ArrayList<CiudadDTO>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			
			statement = conexion.getSQLConexion().prepareStatement(readall);
			statement.setInt(1, 1);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				provincias.add(getCiudadDTO(resultSet));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return provincias;
	}
	
	@Override
	public List<CiudadDTO> filter(int id_provincia) {
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		
		ArrayList<CiudadDTO> provincias = new ArrayList<CiudadDTO>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			
			statement = conexion.getSQLConexion().prepareStatement(readall);
			statement.setInt(1, id_provincia);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				provincias.add(getCiudadDTO(resultSet));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return provincias;
	}
	
	private CiudadDTO getCiudadDTO(ResultSet resultSet) throws SQLException
	{
		int id = resultSet.getInt("id");
		String nombre = resultSet.getString("nombre");
		int id_provincia = resultSet.getInt("id_provincia");
		return new CiudadDTO(id, nombre ,id_provincia);
	}


}
