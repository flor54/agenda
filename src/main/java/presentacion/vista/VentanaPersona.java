package presentacion.vista;



import javax.swing.JFormattedTextField ;
import java.text.SimpleDateFormat;
import java.util.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import dto.CiudadDTO;
import dto.ProvinciaDTO;

public class VentanaPersona extends JFrame 
{
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txtNombre;
	private JTextField txtTelefono;
	private JTextField txtEmail;
	private JFormattedTextField fechaCumpleanios;
	private JButton btnAgregarPersona;
	private static VentanaPersona INSTANCE;
	
	
	
	private int provinciaSeleccionada;
	private int ciudadSeleccionada;
	private List<ProvinciaDTO> listaProvincia;
	private List<CiudadDTO> listaCiudad;
	
	private JComboBox<String> jComboBox1;
	private JComboBox<String> jComboBox2;
	private JComboBox<String> jComboBox3;
	
	
	public static VentanaPersona getInstance()
	{
		if(INSTANCE == null)
		{
			INSTANCE = new VentanaPersona(); 	
			return new VentanaPersona();
		}
		else
			return INSTANCE;
	}

	public JComboBox<String> getjComboBox3() {
		return jComboBox3;
	}

	public void setjComboBox3(JComboBox<String> jComboBox3) {
		this.jComboBox3 = jComboBox3;
	}

	public int getProvinciaSeleccionada() {
		return provinciaSeleccionada;
	}

	public void setProvinciaSeleccionada(int provinciaSeleccionada) {
		this.provinciaSeleccionada = provinciaSeleccionada;
	}

	public int getCiudadSeleccionada() {
		return ciudadSeleccionada;
	}

	public void setCiudadSeleccionada(int ciudadSeleccionada) {
		this.ciudadSeleccionada = ciudadSeleccionada;
	}

	public JTextField getEmail() {
		return txtEmail;
	}

	public void setEmail(JTextField email) {
		this.txtEmail = email;
	}

	public JComboBox<String> getjComboBox1() {
		return jComboBox1;
	}

	public void setjComboBox1(JComboBox<String> jComboBox1) {
		this.jComboBox1 = jComboBox1;
	}

	public JFormattedTextField getFechaCumpleanios() {
		
		return fechaCumpleanios;
	}

	public void setFechaCumpleanios(JFormattedTextField fechaCumpleanios) {
		this.fechaCumpleanios = fechaCumpleanios;
	}

	private VentanaPersona() 
	{
		super();
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 700, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(100, 100, 700, 500);
		contentPane.add(panel);
		panel.setLayout(null);
//----------labels-------------------------------------------------------------		


		JLabel lblNombreYApellido = new JLabel("Nombre y apellido:");
		lblNombreYApellido.setBounds(10, 11, 113, 14);
		panel.add(lblNombreYApellido);
		
		JLabel lblTelfono = new JLabel("Telefono:");
		lblTelfono.setBounds(10, 52, 113, 14);
		panel.add(lblTelfono);
		
		JLabel lblEmail = new JLabel("Email:");
		lblEmail.setBounds(10, 100, 113, 14);
		panel.add(lblEmail);
		
		JLabel lblCumpleanios = new JLabel("Fecha de Nacimiento (yyyy/mm/dd):");
		lblCumpleanios.setBounds(10, 150, 113, 14);
		panel.add(lblCumpleanios);
		
		JLabel lblPais = new JLabel("Pais:");
		lblPais.setBounds(10, 170, 113, 14);
		panel.add(lblPais);
		
		JLabel lblProvincia = new JLabel("Provincia:");
		lblProvincia.setBounds(10, 200, 113, 14);
		panel.add(lblProvincia);
		
		JLabel lblCiudad = new JLabel("Ciudad:");
		lblCiudad.setBounds(10, 230, 113, 14);
		panel.add(lblCiudad);
		
//-----------FIELD------------------------------------------------------------------

		
		txtNombre = new JTextField();
		txtNombre.setBounds(133, 8, 164, 20);
		panel.add(txtNombre);
		txtNombre.setColumns(10);
		
		txtTelefono = new JTextField();
		txtTelefono.setBounds(133, 49, 164, 20);
		panel.add(txtTelefono);
		txtTelefono.setColumns(10);
		
		
		txtEmail = new JTextField();
		txtEmail.setBounds(133, 100, 164, 20);
		panel.add(txtEmail);
		txtEmail.setColumns(10);
		
		DateFormat format = new SimpleDateFormat("yyyy/mm/dd");
		fechaCumpleanios = new JFormattedTextField(format);
		fechaCumpleanios.setBounds(133, 150, 164, 20);
		panel.add(fechaCumpleanios);
		fechaCumpleanios.setColumns(10);
		
		jComboBox2 = new JComboBox<>();
		jComboBox2.setBounds(133, 170, 164, 20);
		jComboBox2.setModel(new DefaultComboBoxModel<>(new String[] {"Argentina"}));
		panel.add(jComboBox2);

		jComboBox1 = new JComboBox<>();
		jComboBox1.setBounds(133, 200, 164, 20);
		panel.add(jComboBox1);
		
		jComboBox3 = new JComboBox<>();
		jComboBox3.setBounds(133, 230, 164, 20);
		panel.add(jComboBox3);
		
// --------------------------------------------------------------------------------------
		btnAgregarPersona = new JButton("Agregar");
		btnAgregarPersona.setBounds(133, 250, 89, 30);
		panel.add(btnAgregarPersona);
		

		
		this.setVisible(false);
	}
	
	public void mostrarVentana()
	{
		this.setVisible(true);
	}
	
	public JTextField getTxtNombre() 
	{
		return txtNombre;
	}

	public JTextField getTxtTelefono() 
	{
		return txtTelefono;
	}

	public JButton getBtnAgregarPersona() 
	{
		return btnAgregarPersona;
	}
	
	
	public void llenarComboProvincias(List<ProvinciaDTO> provincias) {
		String [] aux = new String[provincias.size()];
		this.listaProvincia = provincias;
		int i = 0;
		for (ProvinciaDTO p : provincias) {
			aux[i] = p.getNombre();
			i++;
		}
		jComboBox1.setModel(new DefaultComboBoxModel<>(aux));
		jComboBox1.addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent evt) {
		        capturarProvincia(evt);
		    }
		});
	}
	public void llenarComboCiuadades(List<CiudadDTO> ciudades) {
		String [] aux = new String[ciudades.size()];
		this.listaCiudad = ciudades;
		int i = 0;
		for (CiudadDTO c : ciudades) {
			aux[i] = c.getNombre();
			i++;
		}
		jComboBox3.setModel(new DefaultComboBoxModel<>(aux));
		jComboBox3.addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent evt) {
		        capturarCiudad(evt);
		    }
		});
	}
	
	protected void capturarCiudad(ActionEvent evt)
	{
	    if(jComboBox3.getSelectedItem().toString() != null)
	    {
	    	String seleccion = jComboBox3.getSelectedItem().toString();
			for (CiudadDTO c : this.listaCiudad)
			{
				if(c.getNombre() == seleccion)
				{
					this.ciudadSeleccionada = c.getId();
					System.out.println(c.getId());
					
				}
			}
	    	
	    }
	}
	
	protected void capturarProvincia(ActionEvent evt)
	{
	    if(jComboBox1.getSelectedItem().toString() != null)
	    {
	    	String seleccion = jComboBox1.getSelectedItem().toString();
			for (ProvinciaDTO p : this.listaProvincia)
			{
				
				if(p.getNombre().equals(seleccion))
				{
					this.provinciaSeleccionada = p.getId();
					System.out.println(p.getId());
				}
			}
	    	
	    }
	}

	public void cerrar()
	{
		this.txtNombre.setText(null);
		this.txtTelefono.setText(null);
		this.dispose();
	}
	
}

