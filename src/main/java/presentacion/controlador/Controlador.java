package presentacion.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Date;
import java.util.List;

import modelo.Agenda;
import presentacion.reportes.ReporteAgenda;
import presentacion.vista.VentanaPersona;
import presentacion.vista.Vista;
import dto.CiudadDTO;
import dto.PersonaDTO;
import dto.ProvinciaDTO;

public class Controlador implements ActionListener
{
		private Vista vista;
		private List<PersonaDTO> personasEnTabla;
		private List<ProvinciaDTO> ProvinciasenTabla;
		private VentanaPersona ventanaPersona; 
		private Agenda agenda;
		
		
		
		
		public Controlador(Vista vista, Agenda agenda)
		{
			this.vista = vista;
			this.vista.getBtnAgregar().addActionListener(a->ventanaAgregarPersona(a));
			this.vista.getBtnBorrar().addActionListener(s->borrarPersona(s));
			this.vista.getBtnReporte().addActionListener(r->mostrarReporte(r));
			this.ventanaPersona = VentanaPersona.getInstance();
			this.ventanaPersona.getBtnAgregarPersona().addActionListener(p->guardarPersona(p));
			this.agenda = agenda;
		}
		
		private void ventanaAgregarPersona(ActionEvent a)
		{
			this.ProvinciasenTabla = this.agenda.obtenerProvincias();
			this.ventanaPersona.llenarComboProvincias( this.ProvinciasenTabla);
			this.ventanaPersona.mostrarVentana();
			
			this.ventanaPersona.getjComboBox1().addActionListener(new ActionListener(){
			    public void actionPerformed(ActionEvent evt) {
			        cargarLocalidades(evt);
			    }
			});
			
			
		}
		
		private void cargarLocalidades(ActionEvent evt) {
			List<CiudadDTO> ciudades = this.agenda.obtenerCiudades(this.ventanaPersona.getProvinciaSeleccionada());
			// System.out.println(this.ventanaPersona.getProvinciaSeleccionada());
			
			this.ventanaPersona.llenarComboCiuadades(ciudades);
		}
		 
		private void guardarPersona(ActionEvent p) {
			String nombre = this.ventanaPersona.getTxtNombre().getText();
			String tel = ventanaPersona.getTxtTelefono().getText();
			String email = ventanaPersona.getEmail().getText();
			String fechaCumpleanios = ventanaPersona.getFechaCumpleanios().getText();
			@SuppressWarnings("deprecation")
			Date fechaCumpleaniosDate = new Date(fechaCumpleanios);
			PersonaDTO nuevaPersona = new PersonaDTO(0, nombre, tel,email,fechaCumpleaniosDate);
			this.agenda.agregarPersona(nuevaPersona);
			this.refrescarTabla();
			this.ventanaPersona.cerrar();
		}

		private void mostrarReporte(ActionEvent r) {
			ReporteAgenda reporte = new ReporteAgenda(agenda.obtenerPersonas());
			reporte.mostrar();	
		}

		public void borrarPersona(ActionEvent s)
		{
			int[] filasSeleccionadas = this.vista.getTablaPersonas().getSelectedRows();
			for (int fila : filasSeleccionadas)
			{
				this.agenda.borrarPersona(this.personasEnTabla.get(fila));
			}
			
			this.refrescarTabla();
		}
		
		public void inicializar()
		{
			this.refrescarTabla();
			this.vista.show();
		}
		
		private void refrescarTabla()
		{
			this.personasEnTabla = agenda.obtenerPersonas();
			this.vista.llenarTabla(this.personasEnTabla);
		}

		@Override
		public void actionPerformed(ActionEvent e) { }
		
}
